const jwt = require('jsonwebtoken')

exports.loginRequired = async (req, res, next) => {
    try{
        const token = req.headers.authorization
        req.user = jwt.verify(token, process.env.JWT_SECRET_KEY)    
        next()
    }catch(err){
        res.json({status: false, error: err.message})
    }
    
}