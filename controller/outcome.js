module.exports = (status, message, data = null) => {
    if (status == false ){
        const result = {
            status: status,
            error: message
        }
        return result
    }
    const result = {
        status: status,
        message: message,
        data: data
    }
    if (data == null){
        delete result.data
    }
    return result
}