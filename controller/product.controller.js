const Supplier = require('../database/models/supplier')
const Product = require('../database/models/product')
const outcome = require('./outcome')

exports.getProduct = async (req, res) => {
    try{
        const product = await Product.find()
        const supplier = await Supplier.find()
        res.status(200).json(outcome(true, 'product data recieved', {product, supplier}))
    }catch(err){
        console.log(err)
        res.json(outcome(false, err.message))
    }
}

exports.addProduct = async (req, res) => {
    try{
        const {name, stock, supplierId} = req.body
        let product = await Product.findOne({name})
        if (product !== null) throw new Error('product already registered')
        product = new Product({
            name,
            stock, 
            supplierId
        })
        const response = await product.save()
        res.status(200).json(outcome(true, 'new product added', response))
    }catch(err){
        console.log(err)
        res.json(outcome(false. err.message))
    }
}

exports.editProduct = async (req, res) => {
    try{
        const {id, name, stock, supplierId} = req.body
        let product = await Product.findByIdAndUpdate(id, { $set: { 
            name: name,
            stock: stock,
            supplierId: supplierId
        }})
        res.status(200).json(outcome(true, 'data product edited', {id: product.id, name, stock, supplierId}))
    }catch(err){
    console.log(err)
    res.json(outcome(false, err.message))
    }
}

exports.deleteProduct = async (req, res) => {
    try{
        const {id} = req.body
        const product = await Product.findByIdAndRemove(id)
        res.status(200).json(outcome(true, 'this product data is no more', product))
    }catch(err){
    console.log(err)
    res.json(outcome(false, err.message))
    }
}
