const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const mongoose = require('mongoose')

const User = require('../database/models/user')
const outcome = require('./outcome')

exports.login = async (req, res) => {
    try{
        const {username, password} = req.body
        console.log(req.body)
        const user = await User.findOne({username})
        if (user==null) throw new Error('no user found')
        const auth = await bcrypt.compare(password, user.password)
        if (!auth) throw new Error('your password is incorrect')
        data = {
            id: user._id,
            username: user.username
        }
        const token = jwt.sign(data,process.env.JWT_SECRET_KEY,{expiresIn: '2h'})
        res.status(200).json(outcome(true, 'login success', {
            username: username,
            token: token
        }))
    }catch(err){
        console.log(err)
        res.json(outcome(false, err.message))
    }
}

exports.register = async (req, res) =>{
    try{
        const {username, password} = req.body
        let user = await User.findOne({username})
        if (user !== null) throw new Error('username is taken')
        const hash = await bcrypt.hash(password, Number(process.env.SALT))
        user = new User({
            username: username,
            password: hash
        })
        const response = await user.save()
        console.log(response._id)
        res.status(200).json(outcome(true, 'user created', {id: response._id, username}))
    }catch(err){
        console.log(err)
        res.json(outcome(false, err.message))
    }
}