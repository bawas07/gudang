const Supplier = require('../database/models/supplier')
const outcome = require('./outcome')

exports.getSupplier = async (req, res) => {
    try{
        const supplier = await Supplier.find()
        res.status(200).json(outcome(true, 'supplier data recieved', supplier))
    }catch(err){
        console.log(err)
        res.json(outcome(false, err.message))
    }
}

exports.addSupplier = async (req, res) => {
    try{
        const {name, address, phone_number} = req.body
        let supplier = await Supplier.findOne({name})
        if (supplier !== null) throw new Error('supplier already registered')
        supplier = new Supplier({
            name,
            address, 
            phone_number
        })
        const response = await supplier.save()
        res.status(200).json(outcome(true, 'new supplier added', response))
    }catch(err){
        console.log(err)
        res.json(outcome(false. err.message))
    }
}

exports.editSupplier = async (req, res) => {
    try{
        const {id, name, address, phone_number} = req.body
        let supplier = await Supplier.findByIdAndUpdate(id, { $set: { 
            name: name,
            address: address,
            phone_number: phone_number
        }})
        res.status(200).json(outcome(true, 'data supplies edited', {id: supplier.id, name, address, phone_number}))
    }catch(err){
    console.log(err)
    res.json(outcome(false, err.message))
    }
}

exports.deleteSupplier = async (req, res) => {
    try{
        const {id} = req.body
        const supplier = await Supplier.findByIdAndRemove(id)
        res.status(200).json(outcome(true, 'this supplier data is no more', supplier))
    }catch(err){
    console.log(err)
    res.json(outcome(false, err.message))
    }
}
