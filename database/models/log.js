const mongoose = require('mongoose')

const logSchema = new mongoose.Schema({
    total: Number,
    message: String,
    productId: String
})

module.exports = mongoose.model('Log', logSchema)