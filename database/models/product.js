const mongoose = require('mongoose')

const productSchema = new mongoose.Schema({
    name: String,
    stock: Number,
    supplierId: String
})

module.exports = mongoose.model('Product', productSchema)