require('dotenv').config()
const mongoose = require('mongoose')
const User = require('../models/user')
const bcrypt = require('bcrypt')
const salt = Number(process.env.SALT)



const createUser = async (username, password='123') => {
    try{
        mongoose.connect(process.env.MONGODB, { useNewUrlParser: true })
        hash = await bcrypt.hash(password, salt)
        data = {
            username: username,
            password: hash
        }
        const user = new User(data)
        // console.log(data)
        const res = await user.save()
        console.log('data saved')
        console.log(res)
        mongoose.connection.close()
    }catch(err){
        console.log(err)
    }
    
}  

createUser('admin')