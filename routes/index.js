const express = require('express');
const router = express.Router();
const {login, register} = require('../controller/reglog.controller')

/* GET home page. */
router.get('/', function(req, res, next) {
  res.red('index', { title: 'Gudang' });
});

router.post('/login', login)

router.post('/register', register)

module.exports = router;
