const express = require('express')
const router = express.Router()
const {addProduct, getProduct, editProduct, deleteProduct} = require('../controller/product.controller')

/* GET home page. */
router.get('/', getProduct)

router.post('/', addProduct)

router.patch('/', editProduct)

router.delete('/', deleteProduct)

module.exports = router
