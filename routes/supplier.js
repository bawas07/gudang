const express = require('express')
const router = express.Router()
const {addSupplier, getSupplier, editSupplier, deleteSupplier} = require('../controller/supplier.controller')

/* GET home page. */
router.get('/', getSupplier)

router.post('/', addSupplier)

router.patch('/', editSupplier)

router.delete('/', deleteSupplier)

module.exports = router
